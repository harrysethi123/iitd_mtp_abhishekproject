package edu.iitd.cse.tsf.solrUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
//import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import edu.iitd.cse.tsf.annotation.TextAnnotatorKBP;
import edu.iitd.cse.tsf.models.SolrBO;
import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

public class FetchDataFromSolr {

	private String solrURLWebDoc;
	private String solrURLNewsWire;
	private String solrURLDiscussionForum;	

	SolrServer serverNewsWire;

	public void setSolrURL(String solrURLWebDoc, String solrURLNewsWire, String solrURLDiscussionForum) {
		this.solrURLWebDoc = solrURLWebDoc;
		this.solrURLNewsWire = solrURLNewsWire;
		this.solrURLDiscussionForum = solrURLDiscussionForum;
	}


	/**
	 * 
	 * @return
	 */
	public String getSolrURLWebDoc() {
		return solrURLWebDoc;
	}


	/**
	 * 
	 * @param solrURLWebDoc
	 */
	public void setSolrURLWebDoc(String solrURLWebDoc) {
		this.solrURLWebDoc = solrURLWebDoc;
	}


	/**
	 * 
	 * @return
	 */
	public String getSolrURLNewsWire() {
		return solrURLNewsWire;
	}


	/**
	 * 
	 * @param solrURLNewsWire
	 */
	public void setSolrURLNewsWire(String solrURLNewsWire) {
		this.solrURLNewsWire = solrURLNewsWire;
		serverNewsWire = new HttpSolrServer(solrURLNewsWire);
	}


	/**
	 * 
	 * @return
	 */
	public String getSolrURLDiscussionForum() {
		return solrURLDiscussionForum;
	}


	/**
	 * 
	 * @param solrURLDiscussionForum
	 */
	public void setSolrURLDiscussionForum(String solrURLDiscussionForum) {
		this.solrURLDiscussionForum = solrURLDiscussionForum;
	}


	/**
	 * 
	 * @param solrQuery
	 * @param start
	 * @param numRows
	 * @return
	 */
	public List<SolrBO> getSolrResultsWebDoc(String solrQuery, int start, int numRows) {

		List<SolrBO> solrBOList = new ArrayList<SolrBO>();

		SolrServer serverWebDoc = new HttpSolrServer(solrURLWebDoc);
		SolrQuery query = new SolrQuery();
		query.setRows(numRows);
		query.setQuery(solrQuery);
		query.setStart(start);

		QueryResponse response =null;
		try {
			response = serverWebDoc.query(query);
		} catch (SolrServerException e) {
			System.out.println("Error fetching results for query "+query+"\t"+start+"\t"+numRows);
			e.printStackTrace();
		}

		if(response != null) {

			SolrDocumentList documentList = response.getResults();
			for (SolrDocument solrDocument : documentList) {

				SolrBO solrBO = new SolrBO();
				solrBO.setId(solrDocument.get("id").toString());
				solrBO.setText(solrDocument.get("webdoc").toString());

				solrBOList.add(solrBO);
			}

		}

		return solrBOList;
	}

	public List<SolrBO> getSolrResultsNewsWire(String solrQuery, int start, int numRows) {
		List<SolrBO> solrBOList = new ArrayList<SolrBO>();

		//SolrServer serverNewsWire = new HttpSolrServer(solrURLNewsWire);
		SolrQuery query = new SolrQuery();
		query.setRows(numRows);
		query.setQuery(solrQuery);
		query.setStart(start);

		QueryResponse response =null;
		try {
			response = serverNewsWire.query(query);
		} catch (SolrServerException e) {
			System.out.println("Error fetching NewsWire results for query "+query+"\t"+start+"\t"+numRows);
			e.printStackTrace();
		}

		if(response != null) {

			SolrDocumentList documentList = response.getResults();
			for (SolrDocument solrDocument : documentList) {

				SolrBO solrBO = new SolrBO();
				solrBO.setId(solrDocument.get("id").toString());
				solrBO.setText(solrDocument.get("doc").toString());

				solrBOList.add(solrBO);
			}

		}
		//serverNewsWire.shutdown();

		return solrBOList;
	}

	public List<SolrBO> getSolrResultsDiscussionForum(String solrQuery, int start, int numRows) {
		List<SolrBO> solrBOList = new ArrayList<SolrBO>();

		SolrServer serverDiscussionForum = new HttpSolrServer(solrURLDiscussionForum);
		SolrQuery query = new SolrQuery();
		query.setRows(numRows);
		query.setQuery(solrQuery);
		query.setStart(start);

		QueryResponse response =null;
		try {
			response = serverDiscussionForum.query(query);
		} catch (SolrServerException e) {
			System.out.println("Error fetching Discussion Forum results for query "+query+"\t"+start+"\t"+numRows);
			e.printStackTrace();
		}

		if(response != null) {

			SolrDocumentList documentList = response.getResults();
			for (SolrDocument solrDocument : documentList) {

				SolrBO solrBO = new SolrBO();
				solrBO.setId(solrDocument.get("id").toString());
				solrBO.setText(solrDocument.get("post").toString());

				solrBOList.add(solrBO);
			}

		}

		return solrBOList;
	}


	//public static List<String> splitSentence(FetchDataFromSolr dataFromSolr,ExtractionEngine engine, String entity, String slotFill) {
	public static void splitSentence(FetchDataFromSolr dataFromSolr) throws Exception{
		//---------MODIFICATIONS DONE BY HARINDER --- START
		
		//List<String> sentencesList = new ArrayList<String>();
		
		//helper(dataFromSolr, "Chairman");
		//helper(dataFromSolr, "Co-founder");

		/*helper(dataFromSolr, "Chairperson");
		helper(dataFromSolr, "CEO");
		helper(dataFromSolr, "Administrator");
		helper(dataFromSolr, "Chancellor");
		helper(dataFromSolr, "Lecturer");
		helper(dataFromSolr, "Minister");
		helper(dataFromSolr, "Ambassador");
		helper(dataFromSolr, "Emperor");
		helper(dataFromSolr, "Admirer");
		helper(dataFromSolr, "Director");
		helper(dataFromSolr, "Governor");
		helper(dataFromSolr, "Manager");
		helper(dataFromSolr, "Neighbor");
		helper(dataFromSolr, "Chief");
		helper(dataFromSolr, "Cofounder");
		helper(dataFromSolr, "Executive");
		helper(dataFromSolr, "Developer");
		helper(dataFromSolr, "Prime Minister");
		helper(dataFromSolr, "Cousin");
		helper(dataFromSolr, "Astronomer");
		helper(dataFromSolr, "Cartoonist");
		helper(dataFromSolr, "Comedian");
		helper(dataFromSolr, "Engineer");
		helper(dataFromSolr, "Cricketer");
		helper(dataFromSolr, "Magician");
		helper(dataFromSolr, "Musician");
		helper(dataFromSolr, "Sportsman");
		helper(dataFromSolr, "Chef");
		helper(dataFromSolr, "Architect");
		helper(dataFromSolr, "Actor");
		helper(dataFromSolr, "Actress");
		helper(dataFromSolr, "Author");
		helper(dataFromSolr, "Player");
		helper(dataFromSolr, "Scientist");
		helper(dataFromSolr, "Coach");
		
		

		
		
		helper(dataFromSolr, "chairperson");
		helper(dataFromSolr, "administrator");
		helper(dataFromSolr, "chancellor");
		helper(dataFromSolr, "lecturer");
		helper(dataFromSolr, "minister");
		helper(dataFromSolr, "ambassador");
		helper(dataFromSolr, "emperor");
		helper(dataFromSolr, "admirer");
		helper(dataFromSolr, "director");
		helper(dataFromSolr, "governor");
		helper(dataFromSolr, "manager");
		helper(dataFromSolr, "neighbor");
		helper(dataFromSolr, "chief");
		helper(dataFromSolr, "cofounder");
		helper(dataFromSolr, "executive");
		helper(dataFromSolr, "developer");
		helper(dataFromSolr, "prime minister");
		helper(dataFromSolr, "cousin");
		helper(dataFromSolr, "astronomer");
		helper(dataFromSolr, "cartoonist");
		helper(dataFromSolr, "comedian");
		helper(dataFromSolr, "engineer");
		helper(dataFromSolr, "cricketer");
		helper(dataFromSolr, "magician");
		helper(dataFromSolr, "musician");
		helper(dataFromSolr, "sportsman");
		helper(dataFromSolr, "chef");
		helper(dataFromSolr, "architect");
		helper(dataFromSolr, "actor");
		helper(dataFromSolr, "actress");
		helper(dataFromSolr, "author");
		helper(dataFromSolr, "player");
		helper(dataFromSolr, "scientist");
		helper(dataFromSolr, "coach");*/
		
		helper(dataFromSolr, " ");
		
		//---------MODIFICATIONS DONE BY HARINDER --- END
		
		/*List<String> sentencesList = new ArrayList<String>();
		
		List<SolrBO> otherTextHits = dataFromSolr.getSolrResultsNewsWire("doc:\""+entity+"\" AND doc:\""+slotFill+"\"",0, 100);
		for (SolrBO solrBO2 : otherTextHits) {

			String text = solrBO2.getText();
			sentencesList.add(text);
			Annotation annotations = engine.annotator.performAnnotations(text);
			List<String> splittedSentence = engine.annotator.getSplittedSentence(annotations);
			for (String string : splittedSentence) {

				if(string.contains(entity) && string.contains(slotFill)){
					otherSentences.add(string);
				}
			}

		}

		return null;*/
	}
	//author --- Harinder
	private static Pattern numberPat, yearPat;
	private static StanfordCoreNLP pipeline;
	private static Properties prop;
	
	//author --- Harinder
	private static boolean isNumber(String token) {
		return numberPat.matcher(token.toString()).find();
	}
	//author --- Harinder
	private static boolean isYear(String token) {
		return yearPat.matcher(token).find();
	}
	
	//author --- Harinder
	private static boolean isReqNumber(String str) throws Exception {
		Map<String, String> nerMap = getNerTags(str);
		
		Annotation doc = new Annotation(str);
		pipeline.annotate(doc);
		List<CoreMap> sentences = doc.get(SentencesAnnotation.class);
		for (CoreMap sentence : sentences) {
			for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
				String tokenStr = token.get(TextAnnotation.class);
				if (isNumber(tokenStr) && !isDate(tokenStr,nerMap)) {
					//System.out.println(tokenStr + "- Yes");
					return true;
				}
			}
		}
		return false;
	}
	
	//author --- Harinder
	private static boolean isDate(String token, Map<String, String> nerTagMap) {
		String nerTag = nerTagMap.get(token);
		
		if(nerTag == null) { System.out.println("this shall be rare..returning true"); return false; }
		if(nerTag.equals("DATE") || nerTag.equals("TIME")) return true;
		
		return false;
	}
	
	
	//author --- Harinder
	private static Map<String, String> getNerTags(String str) throws Exception {

	    String serializedClassifier = "classifiers/english.muc.7class.distsim.crf.ser.gz";

	    AbstractSequenceClassifier<CoreLabel> classifier = CRFClassifier.getClassifier(serializedClassifier);

	    String nerTagged = classifier.classifyToString(str, "slashTags", false);
	    String[] nerTaggedArr = nerTagged.split(" ");
	    
	    Map<String, String> nerMap = new HashMap<String, String>();
	    
	    for(String word : nerTaggedArr) {
	    	String[] tagSplit = word.split("/");
	    	if(tagSplit.length<2) continue;
	    	nerMap.put(tagSplit[0], tagSplit[1]);
	    }
	    
	    return nerMap;
	  }


	//author --- Harinder
	private static void helper(FetchDataFromSolr dataFromSolr, String entity) throws Exception {
		List<String> splittedSentences = new ArrayList<String>();
		List<SolrBO> textHits = new ArrayList<SolrBO>();

		//textHits.addAll(dataFromSolr.getSolrResultsNewsWire("doc:\"" + entity
			//	+ "\"", 1, 800));
		
		textHits.addAll(dataFromSolr.getSolrResultsNewsWire("*:*", 1, 3000));
		
		helper_init();
		
		
		for (SolrBO solrBO : textHits) {
			
			if(splittedSentences.size() == 200) break;

			String text = solrBO.getText();

			TextAnnotatorKBP annotator = new TextAnnotatorKBP();
			Annotation annotation = annotator.performAnnotations(text);

			List<String> splittedSentence = annotator
					.getSplittedSentence(annotation);
			for (String string : splittedSentence) {

				if (string.contains(entity)) {
					// if (string.toUpperCase().contains(entity.toUpperCase()) {
					//if (string.matches(".*\\d.*")) { // contains a number
					if(string.matches(".*\\d.*") && isReqNumber(string)) {
						splittedSentences.add(string);
					}
				}
			}

		}

		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				"output.txt", true)));
		for (String string : splittedSentences) {
			out.println(string.replace('\n', ' ').replaceAll("&apos;", "'").replaceAll("&quot;", "\""));
		}

		out.flush();
		out.close();

		//System.out.println("----------DONE-" + entity + ".......");
		PrintWriter out2 = new PrintWriter(new BufferedWriter(new FileWriter(
				"log.txt", true)));
		out2.println("----------DONE-" + entity + ".......");
		out2.flush();
		out2.close();
	}

	//author --- Harinder
	private static void helper_init() {
		numberPat = Pattern.compile("^[\\+-]?\\d+([,\\.]\\d+)*([eE]-?\\d+)?$");
		yearPat = Pattern.compile("^19[56789]\\d|20[01]\\d$");
		prop = new Properties();
		prop.put("annotators", "tokenize, ssplit");
		pipeline = new StanfordCoreNLP(prop);
	}
	
	
	/*public static void main(String[] args) {
		
		FetchDataFromSolr dataFromSolr = new FetchDataFromSolr();
		dataFromSolr.setSolrURLNewsWire("http://panini2.cse.iitd.ernet.in:9192/solr");
		
		
		int start = 0;
		int end = 100;
		int fetchedDataSize = 0;

		do{
			List<SolrBO> solrResultsNewsWire = dataFromSolr.getSolrResultsNewsWire("doc:\"John\"", start, 100);
			for (SolrBO solrBO : solrResultsNewsWire) {
				System.out.println(solrBO.getId()+"\t"+solrBO.getText());

			}

			fetchedDataSize = solrResultsNewsWire.size();
			start+= 100;
			end+= 100;

		}while(fetchedDataSize > 99);
	}*/
	
	
	public static void main(String[] args) throws Exception {
		//---------MODIFICATIONS DONE BY HARINDER --- START
		
		FetchDataFromSolr dataFromSolr = new FetchDataFromSolr();
		dataFromSolr.setSolrURLNewsWire("http://panini2.cse.iitd.ernet.in:9192/solr");
		dataFromSolr.setSolrURLWebDoc("http://panini2.cse.iitd.ernet.in:9193/solr");
		
		//ExtractionEngine engine = new ExtractionEngine("Resources/KBP/kbpDefault.properties");
		splitSentence(dataFromSolr); 
		
		String string = "Oil prices slipped below $82 a barrel Thursday, pausing from a rally that lifted the commodity to a three-month high this week amid a weakening dollar and an economic report suggesting that hiring in the U.S. remains weak.";
		
		/*helper_init();
		if(string.matches(".*\\d.*") && isReqNumber(string)) {
			System.out.println("true");
		}*/
		
		System.out.println("=========Done with fetching the sentences==========");
		
		
		//---------MODIFICATIONS DONE BY HARINDER --- END

		/*FetchDataFromSolr dataFromSolr = new FetchDataFromSolr();
		dataFromSolr.setSolrURLNewsWire("http://panini2.cse.iitd.ernet.in:9192/solr");

		XStream xstream = new XStream();
		xstream.alias("otherSentence", String.class);
		xstream.alias("Entity", EntityOutputBOXML.class);
		xstream.addImplicitCollection(EntityOutputBOXML.class, "otherSentences");

		ExtractionEngine engine = new ExtractionEngine("Resources/KBP/kbpDefault.properties");

		int start = 0;
		int end = 100;
		int fetchedDataSize = 0;
		int slotfillExtractionCount = 0;

		do{
			List<SolrBO> solrResultsNewsWire = dataFromSolr.getSolrResultsNewsWire("doc:\"Robert\"", start, 100);
			for (SolrBO solrBO : solrResultsNewsWire) {
				System.out.println(solrBO.getId());
				List<EntityOutputBO> outputBOs = engine.processText(solrBO.getText(),false);

				for (EntityOutputBO entityOutputBO : outputBOs) {

					Map<String, List<String>> entitySlotFills = entityOutputBO.getEntitySlotFills();
					Map<String, List<String>> entitySlotFillsSentence = entityOutputBO.getEntitySlotFillsSentence();


					for (Entry<String,List<String>> relMap : entitySlotFills.entrySet()) {


						List<String> sentences = entitySlotFillsSentence.get(relMap.getKey());
						List<String> value = relMap.getValue();

						for (int i=0;i<value.size();i++) {
							System.out.println(value.get(i)+"\t"+sentences.get(i));
							EntityOutputBOXML entityOutputBOXML = new EntityOutputBOXML();
							entityOutputBOXML.setEntity(entityOutputBO.getEntity());
							entityOutputBOXML.setRelationType(relMap.getKey());
							entityOutputBOXML.setSlotfill(value.get(i));
							entityOutputBOXML.setOriginalSentence(sentences.get(i));
							List<String> otherSentences = new ArrayList<String>();
							
							slotfillExtractionCount++;

							String entity = entityOutputBO.getEntity();
							String slotFill = value.get(i);

							List<SolrBO> otherTextHits = dataFromSolr.getSolrResultsNewsWire("doc:\""+entity+"\" AND doc:\""+slotFill+"\"",0, 100);
							System.out.println("\n\nSize of fetched other sentence "+otherTextHits.size());
							for (SolrBO solrBO2 : otherTextHits) {

								String text = solrBO2.getText();
								//otherSentences.add(text);
								Annotation annotations = engine.annotator.performAnnotations(text);
								List<String> splittedSentence = engine.annotator.getSplittedSentence(annotations);
								for (String string : splittedSentence) {

									if(string.toLowerCase().contains(entity) && string.toLowerCase().contains(slotFill)){
										otherSentences.add(string);
									}
								}

							}

							entityOutputBOXML.setOtherSentences(otherSentences);

							System.out.println(xstream.toXML(entityOutputBOXML));
							BufferedWriter writer = new BufferedWriter(new FileWriter("/home/abhishek/SlotFillFiles/Robert/File"+slotfillExtractionCount+".xml"));
							writer.write(xstream.toXML(entityOutputBOXML));
							writer.close();
						}
					}
				}
			}

			fetchedDataSize = solrResultsNewsWire.size();
			start+= 100;
			end+= 100;

		}while(fetchedDataSize > 99);*/

	}
	/**
	 * @param args
	 * @throws SolrServerException 
	 * @throws MalformedURLException 
	 */
	/*public static void main(String[] args) throws SolrServerException, MalformedURLException {
		// TODO Auto-generated method stub
		SolrServer server = new HttpSolrServer("http://10.208.23.201:8983/solr");
		SolrQuery query = new SolrQuery();
		query.setRows(20);
		query.setQuery("*:*");
		//query.setStart(10);

		QueryResponse response = server.query(query);
		SolrDocumentList documentList = response.getResults();

		for (SolrDocument solrDocument : documentList) {
			String id = solrDocument.get("id").toString();
			String string = solrDocument.get("webdoc").toString();
			System.out.println(id);
			System.out.println(string+"\n");
		}

		FetchDataFromSolr dataFromSolr = new FetchDataFromSolr();
		dataFromSolr.setSolrURLWebDoc("http://10.208.23.201:8983/solr");
		dataFromSolr.setSolrURLNewsWire("http://10.237.23.28:8983/solr");
		dataFromSolr.setSolrURLDiscussionForum("http://10.237.23.28:9893/solr");

		List<SolrBO> solrResultsDiscussionForum = dataFromSolr.getSolrResultsDiscussionForum("post:\"Ramazan Bashardost\"", 0, 100);
		for (SolrBO solrBO : solrResultsDiscussionForum) {
			System.out.println(solrBO.getId());
		}

		List<SolrBO> solrResultsNewsWire = dataFromSolr.getSolrResultsNewsWire("doc:\"Ramazan Bashardost\"", 0, 100);
		for (SolrBO solrBO : solrResultsNewsWire) {
			System.out.println(solrBO.getId());
		}

		dataFromSolr.getSolrResultsWebDoc("webdoc:\"Ramazan Bashardost\"", 0, 100);
		for (SolrBO solrBO : solrResultsNewsWire) {
			System.out.println(solrBO.getId());
		}
	}*/

}
